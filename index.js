var express = require('express');
var http = require('http');
var https = require('https');
var bodyParser = require('body-parser')

var app = express();

function leftpadstr (str, len, pad) {
  str = String(str);
  prepend = ""

  var i = -1;

  if (!pad && pad !== 0) pad = " ";

  len = len - str.length;

  while (++i < len) {
    prepend += pad.charAt(i%pad.length);
  }

  return prepend + str;
}


app.use(bodyParser.json()); //supporting json in post bodys
app.use(express.static('public'));


app.post('/', function(req, res) {
	if(!req.body.message || !req.body.padding) {
		res.send('api requires json object with a message and padding value');
	}
	else {
		var padWith = req.body.hasOwnProperty('padWith') ? String(req.body.padWith) : " ";
		var msg = req.body.message;
		var padding = +req.body.padding;

		if (+padding < 9001) {
			msgPadded = leftpadstr(msg, padding, padWith);

			if(msgPadded && (msgPadded.length === padding || msgPadded.length === msg.length) ){
				res.type('application/json');
			 	res.send({
					padded: msgPadded
				}); 
			}
			else {
				res.send('error padding your message');
			}
		}
		else {
			res.send("Sorry, padded messages must be less than 9000 characters");
		}
		
	}
});

app.get('/', function(req, res) {
	res.status(200);
	res.send('This is a leftpad microservice'+
		'<br><br><strong>Usage:</strong> POST a json object with a <i>message</i> and <i>padding</i> field to receive a json response with a <i>padded</i> field'+
		'<br>You may also optionally include a <i>padWith</i> field to set what the message is padded with (default is a space)'
	);
});


app.listen(process.env.PORT || 4730);